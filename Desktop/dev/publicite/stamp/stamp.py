from PyPDF2 import PdfFileReader, PdfFileWriter
import sys, os
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QVBoxLayout
from PyQt5.QtCore import Qt


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        #base_path = sys._MEIPASS
        base_path = os.path.dirname(sys.argv[0])
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)



WATERMARK_FILE_TOP = resource_path('watermark/wm.pdf')
WATERMARK_FILE_BTM = resource_path('watermark/wmb.pdf')

def watermark_pdf(infile, outfile='output-wm.pdf'):
    output_stream = PdfFileWriter()
    pdf_file = PdfFileReader(open(infile, 'rb'))
    wm_top = PdfFileReader(open(WATERMARK_FILE_TOP, 'rb'))
    wm_btm = PdfFileReader(open(WATERMARK_FILE_BTM, 'rb'))
    no_pages = pdf_file.getNumPages()
    for i in range(no_pages):
        print(i, no_pages)
        #watermark.scaleBy(0.7) 
        page = pdf_file.getPage(i)    
        if i == 0:
            watermark = wm_top.getPage(0)
            page.mergePage(watermark)  
        elif i == no_pages -1:
            watermark = wm_btm.getPage(0)
            page.mergePage(watermark)
        output_stream.addPage(page)

    with open('{0}'.format(resource_path(outfile)), 'wb') as f:
        output_stream.write(f)
    return True
    
    
#GUI  using PyQt6

class ImageLabel(QLabel):
    def __init__(self):
        super().__init__()

        self.setAlignment(Qt.AlignCenter)
        self.setText('\n\n Drop PDF to be watermarked Here \n\n')
        self.setStyleSheet('''
            QLabel{
                border: 4px dashed #aaa
            }
        ''')

    def setPixmap(self, image):
        super().setPixmap(image)

class App(QWidget):
    
    def __init__(self):
        super().__init__()
        self.resize(400, 400)
        self.setAcceptDrops(True)

        mainLayout = QVBoxLayout()

        self.photoViewer = ImageLabel()
        mainLayout.addWidget(self.photoViewer)
        self.setWindowTitle('LM PDF WATERMARK @JP')
        self.setLayout(mainLayout)

    def dragEnterEvent(self, event):
        if event.mimeData():
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        print('drop event')
        if event.mimeData():
            event.setDropAction(Qt.CopyAction)
            files = event.mimeData().urls() 
            for i in range(len(files)):
                file_path = event.mimeData().urls()[i].toLocalFile()
                #print("file path", file_path)
                watermark_pdf(file_path, outfile='{}-wm.pdf'.format(i))

            self.set_image(file_path)

            event.accept()
        else:
            event.ignore()

    def set_image(self, file_path):
        self.photoViewer.setText('\n\n All done ! \n\n')
        #self.photoViewer.setPixmap(QPixmap(file_path))





app = QApplication(sys.argv)
demo = App()
demo.show()
sys.exit(app.exec_())